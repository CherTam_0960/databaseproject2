/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

import java.awt.Event;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Orders {

    public static Orders fromRS(ResultSet rs) {
        Orders order = new Orders();
        try {
            order.setId(rs.getInt("order_id"));
            order.setQty(rs.getInt("order_qty"));
            order.setTotal(rs.getDouble("order_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-mm-dd HH:mm:ss");
            order.setDate(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }

    private int id;
    private Date date;
    private double total;
    private int qty;
    private ArrayList<OrderDetail> ordetDetails;

    public Orders(int id, Date date, double total, int qty, ArrayList<OrderDetail> ordetDetails) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.qty = qty;
        this.ordetDetails = ordetDetails;
    }

    public Orders() {
        this.id = -1;
        qty = 0;
        total = 0;
        ordetDetails = new ArrayList<>();
        date = new Date();
    }

    public void addOrderDetail(OrderDetail orderDetail) {
        ordetDetails.add(orderDetail);
        total = total + orderDetail.getTotal();
        qty = qty + orderDetail.getQty();
    }

    public void addOrderDetail(Product product, String productName, double productPrice, int qty) {
        OrderDetail orderDetail = new OrderDetail(product, productName, productPrice, qty, this);
        this.addOrderDetail(orderDetail);
    }

    public void addOrderDetail(Product product, int qty) {
        OrderDetail orderDetail = new OrderDetail(product, product.getName(), product.getPrice(), qty, this);
        this.addOrderDetail(orderDetail);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", date=" + date + ", total=" + total + ", qty=" + qty + "}";
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<OrderDetail> getOrdetDetails() {
        return ordetDetails;
    }

    public void setOrdetDetails(ArrayList<OrderDetail> ordetDetails) {
        this.ordetDetails = ordetDetails;
    }

    public ArrayList<OrderDetail> OrderDetail() {
        return ordetDetails;
    }

}
